<?php


namespace yii2portal\mediator\components;

use Yii;
use yii2portal\core\components\Widget;

class Article extends Widget
{


    public function init()
    {
        parent::init();
        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();

        $module = Yii::$app->getModule('mediator');

        return <<<EOF
<div class="js-mediator-article">{$content}</div>
<script id="js-mpf-mediator-init" data-counter="{$module->counterId}" data-adaptive="true">
!function(e){function t(t,n){if(!(n in e)){for(var r,a=e.document,i=a.scripts,o=i.length;o--;)if(-1!==i[o].src.indexOf(t)){r=i[o];break}if(!r){r=a.createElement("script"),r.type="text/javascript",r.async=!0,r.defer=!0,r.src=t,r.charset="UTF-8";var d=function(){var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(r,e)};"[object Opera]"==e.opera?a.addEventListener?a.addEventListener("DOMContentLoaded",d,!1):e.attachEvent("onload",d):d()}}}t("//mediator.mail.ru/script/28214324/","_mediator")}(window);
</script>
EOF;

    }
}
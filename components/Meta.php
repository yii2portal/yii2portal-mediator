<?php


namespace yii2portal\mediator\components;

use Yii;
use yii2portal\core\components\Widget;
use yii2portal\news\models\News;

class Meta extends Widget
{


    public function register($item, $value = null)
    {

        if(class_exists('yii2portal\news\models\News') && $item instanceof News){
            $this->view->registerMetaTag([
                'name' => 'mediator',
                'content' => $item->id
            ]);


            foreach ($item->allAuthors as $author) {
                $metaData[] = $author;
            }
            if(!empty($metaData)) {
                $this->view->registerMetaTag([
                    'name' => 'mediator_author',
                    'content' => implode(', ', $metaData)
                ]);
            }
        }elseif(is_string($item)){
            $this->view->registerMetaTag([
                'name' =>  $item,
                'content' => $value
            ]);
        }


    }
}
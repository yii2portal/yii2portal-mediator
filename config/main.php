<?php


return [
    'components'=>[
        'meta'=>[
            'class'=>'yii2portal\mediator\components\Meta'
        ],
        'article'=>[
            'class'=>'yii2portal\mediator\components\Article'
        ]
    ]
];